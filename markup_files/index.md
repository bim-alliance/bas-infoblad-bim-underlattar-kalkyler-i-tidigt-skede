![Bild 1](media/1.jpeg)

**Med hjälp av utrymmeskalkyler kan man göra kostnadskalkyler i ett tidigt skede av byggprocessen. Det räcker med grova nyckeltal. Illustration: Bjerking AB**

# BIM underlättar kalkyler i tidigt skede

> ##### Genom att utnyttja rumsinformation i en BIM-modell kan man framställa en så kallad utrymmeskalkyl i ett byggprojekt. En aktiv kostnadskalkylering i tidigt skede ger snabbare och enklare avstämningar vilket ökar möjligheterna till kostnadsstyrning och jämförelser av flera alternativ.

– MED VÅR METOD SÅ KAN MAN, teoretiskt sett, med en knapptryckning se hur kostnaden ser ut för olika alternativ, säger Jan-Olof Edgar, affärsutvecklare Lean Design på Bjerking AB, som lett OpenBIM-projektet ”Informationsöverföring
projektering till kalkyl”. 
​	Vanligtvis kostnadskalkylerar man ganska sent då ritningarna redan är ganska detaljerade. Många gånger ritar man fast sig i en lösning och upptäcker sent att den valda utformningen blir för dyr. Men ju längre man kommit desto svårare att backa. Det finns undersökningar som visar att man i byggsektorn endast prövar 2,8 alternativ innan man bestämmer en lösning. Att man inte utreder fler alternativ
beror på att det blir för dyrt.
​	– Ju tidigare man kan komma rätt desto bättre. Man vill kunna kostnadsstyra mycket tidigare än man gör idag för att se att man är i rätt kostnadshärad och för att snabbt kunna jämföra olika alternativ. Att rita om flera gånger för att man hamnar fel kostnadsmässigt är rent slöseri, vilket man enligt ”Lean-principer” ska reducera.
​	En utrymmeskalkyl, det vill säga att göra kalkyl utifrån utrymmen istället för utifrån byggdelar, kan göras utifrån skisser på olika areor. Kalkylprogrammen som Bygganalys använder har erfarenhetsvärden för olika typhus och olika
typrum.
​	Men någon måste sitta och tolka det som är ritat, en omständlig och tidskrävande process och dessutom källa till fel. Skillnaden nu är att man med hjälp av BSAB-koder– nyckeln för att lyckas med det här arbetet – inte längre behöver tolka det som är ritat eftersom datorn vet vad varje area är för något. Man kan låta BIM-programmet prata med kalkylprogrammet, utan mänsklig tolkning av det som är ritat emellan, vilket gör hela processen enklare och snabbare. Ett paradigmskifte för byggprocessen, menar JanOlof Edgar.

FÖRUTSÄTTNINGEN FÖR ATT TA FRAM den här typen av kalkyler är att man arbetar med BIM-verktyg och skapar utrymmesobjekt. Dessa objekt måste vara bärare av en BSAB-kod, en ”maskinkod” som exempelvis talar om att ett visst utrymme är ett kök. Dessutom krävs kalkylprogram som har register med erfarenhetsvärden som är knutet till de olika utrymmena.
​	– Jag fick skapa en tabell med utrymmen och lägga upp register som jag kunde välja från. Därefter var det inga direkt  svårigheter att göra kalkylen. Bygganalys hade inte färdiga recept för alla typrum utan fick göra lite manuellt arbete, säger Jan-Olof Edgar som menar att ett generellt BIM-problem är att det krävs lite förarbete innan man når de stora fördelarna.

ATT MÄRKA ALLTING MED BSAB-KODER tar sin tid. Men koderna gör det möjligt att använda informationen i CAD-filen eller BIM-filen i andra IT-system. Dock borde programleverantörerna se potentialen i detta och på sikt leverera färdiga register i sina program. Fram tills nu har BSAB-koder främst använts i produktionsskedet av en byggnad, det vill säga för beskrivningar, mängdförteckningar samt kalkyler. Projektörer och förvaltare har idag ingen egentlig egennytta med koderna och därför är till exempel utrymmestabellerna inte fullständigt utvecklade. Det är först med BIM och ”digitala informationsleveranser” som BSAB-systemets stora genombrott kommer. Resultatet av utrymmeskalkylen blev över förväntan. JanOlof Edgar hade accepterat en summa som var plus minus 20 procent jämfört med den verkliga anbudssumman. Nu avvek kalkylen endast sex procent mot anbudssumman.

![Bild 2](media/2.jpeg)

**Produktbestämning enligt BSAB-modellen**

– DET ÄR MYCKET BRA OCH RESULTATET visar att metoden kan vara till stor nytta för kalkyler i tidiga skeden i projekt. Men det krävs fler projekt för att få bättre erfarenhetsvärden och kunna garantera träffsäkerhet. Det stora värdet med projektet är att det visat att den här möjligheten finns när man jobbar i BIM. Det goda exemplets makt är viktig. Jan-Olof Edgar betonar att det inte är någon stor utmaning att göra den här typen av kalkyler. Det som krävs är en investering i tid plus visst kunnande och intresse. 
​	– Sätt igång, är hans uppmaning.